#!/bin/bash

function readFirstValueVariable () {
  echo "Informe o primeiro número:"
  read x

  if [ -z $x ]; then
    readFirstValueVariable
  fi
}

function readSecondValueVariable () {
  echo "Informe o segundo número:"
  read y

  if [ -z $y ]; then
    readSecondValueVariable
  fi
}

while :; do

  readFirstValueVariable
  readSecondValueVariable

  echo "Resultado:"
  echo $(($x*$y))

done